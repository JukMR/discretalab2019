#include "Rii.h"

static int CompararLado(const void * left, const void * right) {
  struct Lado *a = (struct Lado *) left;
  struct Lado *b = (struct Lado *) right;
  if (a->v1 < b->v1) {
      return -1;
  } else if (a->v1 > b->v1) {
      return 1;
  } else {
      return 0;
  }
}

Grafo ConstruccionDelGrafo() {
  FILE *fp = stdin;
  unsigned int n = 0;
  unsigned int m = 0;
  bool found_p_edge = false;

  char tmp;
  bool comentario = false;


  // Empezar a leer lineas
  while (!found_p_edge && ((tmp = fgetc(fp)) != EOF)) {
    // detecta lineas de comentario
    if (tmp == 'c') {
      comentario = true;
    } else if (tmp == '\n') {
      comentario = false;
    } else if (tmp == 'p' && !comentario) {
      if (scanf(" edge %u %u", &n, &m) != 2) {
        printf("%s\n", "FALLO AL CARGAR M Y N");
      }
      found_p_edge = true;
    } else if (tmp != 'p' && !comentario) {
      printf("%s\n", "error en primera linea sin comentario");
      return NULL;
    }
  }
  // Cargar datos en un array de lados
  struct Lado *arr_lados = calloc (2*m, sizeof (struct Lado*));

  //  Compruebo si se aloco bien la memoria, en caso contrario libero todo lo
  //  que haya alocado hasta el mometo y devuelvo NULL
  if (arr_lados == NULL) {
    printf("%s\n", "error al alocar memoria");
    return NULL;
  }

  u32 i = 0;        // iterador del while
  u32 index = 0;    // index del array
  u32 scanu;        // Variable para comprobar estado de scanf

  while ((tmp = fgetc(fp) != EOF) && (i < m)) {
    scanu = scanf(" e %u %u", &(arr_lados[index].v1), &(arr_lados[index].v2));
    if (scanu == 2) {
      arr_lados[index+1].v1 = arr_lados[index].v2;
      arr_lados[index+1].v2 = arr_lados[index].v1;
    } else {
      printf("error de lectura en el lado %u\n", i);
      free(arr_lados);
      return NULL;
    }
    i++;
    index = index + 2;
  }

  if (index < (m * 2)) {
    printf("error de lectura en el lado %u", m+1);
    free(arr_lados);
    return NULL;
  }

  // ---------------------------------------------------------------------
  // ORDENAR EL ARRAY DE LADOS
  // ---------------------------------------------------------------------

  qsort(arr_lados, 2*m, sizeof(struct Lado), CompararLado);

  // ------------------------------------------------------------------------
  // CREAR Y COMPLETAR ARRAY DE VERTICES
  // ------------------------------------------------------------------------

  Grafo grafo = NULL;
  grafo = calloc (1, sizeof (struct grafoSt*));

  //  Compruebo si se aloco bien la memoria, en caso contrario libero todo lo
  //  que haya alocado hasta el mometo y devuelvo NULL
  if (grafo == NULL) {
    printf("%s\n", "error al alocar memoria");
    free(arr_lados);
    return NULL;
  }

  grafo->n_vertices = n;
  grafo->m_lados = m;
  grafo->cant_colores = 0;
  grafo->arr_vertices = calloc(grafo->n_vertices, sizeof(struct Vertice*));

  //  Compruebo si se aloco bien la memoria, en caso contrario libero todo lo
  //  que haya alocado hasta el mometo y devuelvo NULL
  if (grafo->arr_vertices == NULL) {
    printf("%s\n", "error al alocar memoria");
    free(arr_lados);
    free(grafo);
  }

  // Aloca espacio que ocupa cada estructura en el array de vertices
  for (u32 i = 0; i < grafo->n_vertices; i++) {
    grafo->arr_vertices[i] = calloc(1, sizeof(struct Vertice));
    //  Compruebo si se aloco bien la memoria, en caso contrario libero todo lo
    //  que haya alocado hasta el mometo y devuelvo NULL
    if (grafo->arr_vertices[i] == NULL) {
      printf("%s\n", "error al alocar memoria");
      for (u32 j = 0; j < i; j++) {
        free(grafo->arr_vertices[j]);
      }
      free(arr_lados);
      free(grafo->arr_vertices);
      free(grafo);
      return NULL;
    }
  }

  u32 counter = 0;
  u32 j = 0;
  while (j < 2 * grafo->m_lados) {
    if (arr_lados[j].v1 != arr_lados[j+1].v1) {
      grafo->arr_vertices[counter]->nombre = arr_lados[j].v1;     // guardo el nombre del vertice
      grafo->arr_vertices[counter]->color = 0;                    // seteo el color de vertice en cero
      grafo->arr_vertices[counter]->grado = 0;                    // seteo el grado del vertice en cer0
      grafo->arr_vertices[counter]->indice = counter;
      counter++;
    }
    j++;
  }

  // Libera la memoria alocada y devuelve NULL si la cantidad de vertices leidos
  // no es la declarada
  if (counter < grafo->n_vertices) {
    printf("Cantidad de vertices leidos no es la declarada");
    for (u32 i = 0; i < grafo->n_vertices; i++) {
      free(grafo->arr_vertices[i]);
    }
    free(grafo->arr_vertices);
    free(grafo);
    free(arr_lados);
    return NULL;
  }

  // ------------------------------------------------------------------------
  // CREAR Y COMPLETAR ARRAY DE VECINOS DE CADA VERTICE
  // ------------------------------------------------------------------------

  j = 0;
  u32 k = 0;
  counter = 0;
  while ( (j < 2*(grafo->m_lados)) && (k < grafo->n_vertices) ) {
    while (arr_lados[j].v1 == grafo->arr_vertices[k]->nombre) {
      counter++;
      j++;
    }
    grafo->arr_vertices[k]->grado = counter;
    k++;
    counter = 0;
  }

  j = 0;
  k = 0;
  u32 l = 0;
  while (j < 2*grafo->m_lados && k < grafo->n_vertices) {
    // Aloca memoria para el array de vecinos
    grafo->arr_vertices[k]->arr_vecinos = calloc(grafo->arr_vertices[k]->grado,
                                                 sizeof(struct Vertice*));

    //  Compruebo si se aloco bien la memoria, en caso contrario libero todo lo
    //  que haya alocado hasta el mometo y devuelvo NULL
    if (grafo->arr_vertices[k]->arr_vecinos == NULL) {
      printf("%s\n", "error al alocar memoria");
      for (u32 i = 0; i < k; i++) {
        free(grafo->arr_vertices[k]->arr_vecinos);
      }
      for (u32 i = 0; i < grafo->n_vertices; i++) {
        free(grafo->arr_vertices[i]);
      }
      free(grafo->arr_vertices);
      free(grafo);
      free(arr_lados);
      return NULL;
    }

    while (arr_lados[j].v1 == grafo->arr_vertices[k]->nombre) {

      // BUSQUEDA BINARIA

      u32 inf = 0;
      u32 sup = grafo->n_vertices-1;
      bool found = false;
      while (inf <= sup && !found) {
        u32 centro = (sup + inf) / 2; // División entera: se trunca la fracción
        if (grafo->arr_vertices[centro]->nombre == arr_lados[j].v2) {
          grafo->arr_vertices[k]->arr_vecinos[l] = grafo->arr_vertices[centro];
          found = true;
        } else if (arr_lados[j].v2 < grafo->arr_vertices[centro]->nombre) {
          sup = centro - 1;
        }else {
          inf = centro + 1;
        }
      }

      // FIN BUSQUEDA BINARIA

      j++;
      l++;
    }
    k++;
    l = 0;
  }

  free(arr_lados);
  u32 greedy = Greedy(grafo);
  return grafo;
}


void DestruccionDelGrafo(Grafo G) {
  for (u32 i = 0; i < G->n_vertices; i++) {
    free(G->arr_vertices[i]->arr_vecinos);  //  libero el array de vecinos de cada vertice
    free(G->arr_vertices[i]);               //  libero la estructura de cada vertice
  }
  free(G->arr_vertices);                    //  libero el array de vertices
  free(G);                                  //  libero la estructura del grafo
}


Grafo CopiarGrafo(Grafo G) {
  Grafo grafo = NULL;
  //  Alloco memoria para la estructura del grafo
  grafo = calloc (1, sizeof (struct grafoSt*));

  //  Compruebo si se aloco bien la memoria, en caso contrario libero todo lo
  //  que haya alocado hasta el mometo y devuelvo NULL
  if (grafo == NULL) {
    printf("%s\n", "error al alocar memoria");
    return NULL;
  }

  grafo->n_vertices = G->n_vertices;
  grafo->m_lados = G->m_lados;
  grafo->cant_colores = G->cant_colores;
  //  Alloco memoria para el array de vertices del grafo
  grafo->arr_vertices = calloc(grafo->n_vertices, sizeof(struct Vertice*));

  //  Compruebo si se aloco bien la memoria, en caso contrario libero todo lo
  //  que haya alocado hasta el mometo y devuelvo NULL
  if (grafo->arr_vertices == NULL) {
    printf("%s\n", "error al alocar memoria");
    free(grafo);
  }

  for (u32 i = 0; i < grafo->n_vertices; i++) {
    //  Alloco memoria para la estructura de cada vertice
    grafo->arr_vertices[i] = calloc(1, sizeof(struct Vertice));
    //  Compruebo si se aloco bien la memoria, en caso contrario libero todo lo
    //  que haya alocado hasta el mometo y devuelvo NULL
    if (grafo->arr_vertices[i] == NULL) {
      printf("%s\n", "error al alocar memoria");
      for (u32 j = 0; j < i; j++) {
        free(grafo->arr_vertices[j]);
      }
      free(grafo->arr_vertices);
      free(grafo);
    }

    G->arr_vertices[i]->indice = i;   // marcar el indice de todo el array de vertices
  }

  for (u32 i = 0; i < grafo->n_vertices; i++) {
    grafo->arr_vertices[i]->nombre = G->arr_vertices[i]->nombre;
    grafo->arr_vertices[i]->color = G->arr_vertices[i]->color;
    grafo->arr_vertices[i]->grado = G->arr_vertices[i]->grado;
    //  Alloco memoria para el array de vecinos del grafo
    grafo->arr_vertices[i]->arr_vecinos = calloc(grafo->arr_vertices[i]->grado,
                                                 sizeof(struct Vertice*));
    //  Compruebo si se aloco bien la memoria, en caso contrario libero todo lo
    //  que haya alocado hasta el mometo y devuelvo NULL
    if (grafo->arr_vertices[i]->arr_vecinos == NULL) {
      printf("%s\n", "error al alocar memoria");
      for (u32 j = 0; j < i; j++) {
        free(grafo->arr_vertices[j]->arr_vecinos);
      }
      for (u32 j = 0; j < grafo->n_vertices; j++) {
        free(grafo->arr_vertices[j]);
      }
      free(grafo->arr_vertices);
      free(grafo);
      return NULL;
    }


    for (u32 j = 0; j < grafo->arr_vertices[i]->grado; j++) {
      // En que posición de grafo->arr_vertices[] esta G->arr_vertices[i]->arr_vecinos[j]
      grafo->arr_vertices[i]->arr_vecinos[j] =
      grafo->arr_vertices[G->arr_vertices[i]->arr_vecinos[j]->indice];
    }
  }

  return grafo;
}

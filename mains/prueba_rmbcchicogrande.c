#include "Rii.h"

int main() {

	Grafo G = ConstruccionDelGrafo();
	if (G == NULL) {
	  return 1;
	}

	Grafo grafo2 = NULL;
	Grafo grafo3 = NULL;

	u32 result = Greedy(G);
	printf("GREEDYYYYY  %u\n", result);

	if (G) {
	  grafo2 = CopiarGrafo(G);
	  grafo3 = CopiarGrafo(G);
	}

	for (u32 i = 0; i < G->n_vertices; i++) {
	  printf("%u ", NombreDelVertice(G,i));
	  printf("%u ", ColorDelVertice(G,i));
	  printf("%u\n", GradoDelVertice(G,i));
	}

	char swii = SwitchColores(G, 0, 2 );
	printf("%c\n", swii);

	char rmbc = RMBCchicogrande(G);
	printf("RMBCchicogrande es %c\n", rmbc);

	for (u32 i = 0; i < G->n_vertices; i++) {
	  printf("%u ", NombreDelVertice(G,i));
	  printf("%u ", ColorDelVertice(G,i));
	  printf("%u\n", GradoDelVertice(G,i));
	}

	// Destruccion de grafos
	// -----------------------------------------------------------------
	DestruccionDelGrafo(G);
	DestruccionDelGrafo(grafo2);
	DestruccionDelGrafo(grafo3);

	return 0;
}

#include "Rii.h"

int main() {

	Grafo G = ConstruccionDelGrafo();
	if (G == NULL) {
	  return 1;
	}

	Grafo grafo2 = NULL;
	Grafo grafo3 = NULL;

	u32 result = Greedy(G);
	printf("GREEDYYYYY  %u\n", result);

	if (G) {
	  grafo2 = CopiarGrafo(G);
	  grafo3 = CopiarGrafo(G);
	}


	printf("%s\n", "Ahora vamos con grafo2 ");
	printf("%s\n", "Sus datos son: ");

	for (u32 i = 0; i < grafo2->n_vertices; i++) {
	  printf("%u ", NombreDelVertice(grafo2,i));
	  printf("%u ", ColorDelVertice(grafo2,i));
	  printf("%u\n", GradoDelVertice(grafo2,i));
	}

	printf("%s\n", "Antes de OrdenWelshPowell");

	char Welsh = OrdenWelshPowell(grafo2);
	printf("Welsh es %c\n", Welsh);

	printf("%s\n", "Despues de OrdenWelshPowell");

	for (u32 i = 0; i < grafo2->n_vertices; i++) {
	  printf("%u ", NombreDelVertice(grafo2,i));
	  printf("%u ", ColorDelVertice(grafo2,i));
	  printf("%u\n", GradoDelVertice(grafo2,i));
	}

	printf("%s\n", "Ahora vamos con grafo3 ");
	printf("%s\n", "Sus datos son: ");

	for (u32 i = 0; i < grafo3->n_vertices; i++) {
	  printf("%u ", NombreDelVertice(grafo3,i));
	  printf("%u ", ColorDelVertice(grafo3,i));
	  printf("%u\n", GradoDelVertice(grafo3,i));
	}

	char Natural = OrdenNatural(grafo3);
	printf("Natural es %c\n", Natural);

	for (u32 i = 0; i < grafo3->n_vertices; i++) {
	  printf("%u ", NombreDelVertice(grafo3,i));
	  printf("%u ", ColorDelVertice(grafo3,i));
	  printf("%u\n", GradoDelVertice(grafo3,i));
	}

	Welsh = OrdenWelshPowell(grafo3);
	printf("Welsh es %c\n", Welsh);

	for (u32 i = 0; i < grafo3->n_vertices; i++) {
	  printf("%u ", NombreDelVertice(grafo3,i));
	  printf("%u ", ColorDelVertice(grafo3,i));
	  printf("%u\n", GradoDelVertice(grafo3,i));
	}

	printf("%s\n", "ACA EMPIEZO CON SWITCHCOLORES");
	printf("%s\n", "TODOS LOS QUE TIENEN COLOR 0 AHORA SON 3 Y VICE");

	char swii = SwitchColores(grafo3, 0 , 3);

	for (u32 i = 0; i < grafo3->n_vertices; i++) {
	  printf("%u ", NombreDelVertice(grafo3,i));
	  printf("%u ", ColorDelVertice(grafo3,i));
	  printf("%u\n", GradoDelVertice(grafo3,i));
	}

	printf("SWITCH COLORES ES %c\n", swii);
	printf("%s\n", "Termine switch colores ");
	printf("%s\n", " Entro a RMBC ");

	char rmbc = RMBCchicogrande(grafo3);
	printf("RMBCchicogrande es %c\n", rmbc);

	for (u32 i = 0; i < grafo3->n_vertices; i++) {
	  printf("%u ", NombreDelVertice(grafo3,i));
	  printf("%u ", ColorDelVertice(grafo3,i));
	  printf("%u\n", GradoDelVertice(grafo3,i));
	}

	// Destruccion de grafos
	// -----------------------------------------------------------------
	DestruccionDelGrafo(G);
	DestruccionDelGrafo(grafo2);
	DestruccionDelGrafo(grafo3);

	return 0;
}

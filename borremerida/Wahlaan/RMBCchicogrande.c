#include "Rii.h"

 typedef struct Ocurrencia_t {
    u32 color;
    u32 cantidad;
    u32 puesto;
} Ocurrencia ;


static int CompararOcurrencia(const void * left, const void * right) {
      Ocurrencia *a = *(Ocurrencia **) left  ;
      Ocurrencia *b = *(Ocurrencia **) right ;
      if (a->cantidad < b->cantidad) {
          return -1;
      } else if (a->cantidad > b->cantidad) {
          return 1;
      } else {
          return 0;
      }
}

static int Reordenar(const void * left, const void * right) {
      Ocurrencia *a = *(Ocurrencia **) left  ;
      Ocurrencia *b = *(Ocurrencia **) right ;
      if (a->color < b->color) {
          return -1;
      } else if (a->color> b->color) {
          return 1;
      } else {
          return 0;
      }
}

char RMBCchicogrande(Grafo G) {
  /*
  La idea del algoritmo es crear un array de vertices auxiliar (aux_arr)que
  luego será el que devolvamos. Primero recorremos el array de vertices
  contando que cantidad de colores hay de cada uno y eso lo vmaos guardando
  un array de triplas cont_color_arr.

  Luego de esto tenemos la cantidad de ocurrencias de cada color y podemos //
  ordenarla de menor a mayor con qsort.

  Luego de ordenados los colores por su cardinalidad, ahora sabemos el
  orden en que tienen que ir los colores en el nuevo array auxiliar.

  Pero ahora tenemos que poder recorrer el array de vertices y a medida que
  vamos leyendo los vertices saber donde debemos colocarlos. Para esto
  luego de haber ordenado, con el orden de los colores correctos vamos
  recorriendo cont_color_arr y contando cuantas ocurrencias de cada color
  hay para calcular los indices en un array acumulativo.  La idea es que
  luego tengamos los indices de donde debe ir cada color.

  Una vez hecho esto ya tenemos el array auxiliar ordenado
  correspondientemente. Queda solo liberar el array original
  (G->arr_vertices) , re-apuntar G->arr_vertices a aux_arr y liberar toda
  la memoria auxiliar que usamos, como el array acumulativo y el array de
  triplas para ordenar los colores.
  */

  // Alocar memoria para reordenar los vertices

  // Crear mi array auxiliar
  struct Vertice **aux_arr = calloc(G->n_vertices, sizeof(struct Vertice*));
  if (aux_arr == NULL) {
    return '1';
  }

  // Array acumulador para los indices en el auxiliar
  u32 *acum_arr = calloc(G->n_vertices, sizeof(u32));
  if (acum_arr == NULL) {
    free(aux_arr);
    return '1';
  }

  // Array para guardar la aparición de cada color
  Ocurrencia **cont_color_arr = calloc(G->cant_colores, sizeof(Ocurrencia *));
  if (cont_color_arr == NULL) {
    free(acum_arr);
    free(aux_arr);
    return '1';
  }

  // Aloco memoria para cont_color_arr y si falla libero todo lo anterior
  for (u32 i = 0; i < G->cant_colores; i++) {
    cont_color_arr[i] = calloc(1, sizeof(Ocurrencia));
    if (cont_color_arr[i] == NULL) {
      for (u32 j = 0; j < i; j++) {
        free(cont_color_arr[i]);
      }
      free(acum_arr);
      free(aux_arr);
      free(cont_color_arr);
      return '1';
      }
  }

  // Recorrer todos los vertices del grafo
  // Cuento cuantos colores hay y los guardo en el indice correspondiente
  for (u32 i = 0; i < G->n_vertices; i++) {
    (cont_color_arr[G->arr_vertices[i]->color])->cantidad++;
    (cont_color_arr[G->arr_vertices[i]->color])->color =
    G->arr_vertices[i]->color;
  }

  // Ordenar en el nuevo array de menor a mayor a los conjuntos por número de
  // elementos

  // Ordeno los colores por cantidad
  qsort(cont_color_arr, G->cant_colores, sizeof(Ocurrencia *),
        CompararOcurrencia);

  // Llenar el array de acumuladores
  // PRE : Ya tengo el arreglo de colores ordenado de menor a mayor
  u32 contador = 0;
  for (u32 i = 0; i < G->cant_colores; i++) {
    // Aprovecho este ciclo para guardar en que puesto quedó cada color.
    // Los puestos empiezan en 0
    cont_color_arr[i]->puesto = i ;
    contador = contador + cont_color_arr[i]->cantidad;
    acum_arr[i] = contador -1; // contador -1 porque el indice empieza en 0
  }

  // Reordeno los vertices por colores para cargarlos

  qsort(cont_color_arr, G->cant_colores, sizeof(Ocurrencia *), Reordenar);

  // Seteo los indices en la posicion del arreglo de vertices para poder
  // reapuntar el array de vecinos

  for (u32 i = 0; i < G->n_vertices; i++) {
    G->arr_vertices[i]->indice = i;
  }

  // Copio los vertices al array auxiliar en el orden que van
  for (u32 i = 0; i < G->n_vertices; i++) {
    u32 index = cont_color_arr[G->arr_vertices[i]->color]->puesto;
    (aux_arr[acum_arr[index]]) = G->arr_vertices[i];

    // Seteo los vecinos
    for (u32 j = 0; j < G->arr_vertices[i]->grado; j++) {
      (aux_arr[acum_arr[index]])->arr_vecinos[j] =
      G->arr_vertices[G->arr_vertices[i]->arr_vecinos[j]->indice];
    }
    acum_arr[index]--;
  }

  // Terminé. Armo los frees. Libero mi G->arr_vertices viejo

  for (u32 i = 0; i < G->n_vertices; i++) {
    G->arr_vertices[i] = aux_arr[i];
  }

  free(G->arr_vertices);
  free(acum_arr);
  G->arr_vertices = aux_arr;

  for (u32 i = 0; i < G->cant_colores; i++) {
    free (cont_color_arr[i]);
  }

  free(cont_color_arr);

  return '0';
}

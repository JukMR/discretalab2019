#include "Rii.h"

u32 NombreDelVertice(Grafo G, u32 i) {
  return G->arr_vertices[i]->nombre;
}

u32 ColorDelVertice(Grafo G, u32 i) {
  if (i > G->n_vertices) {
    return COLOR_NULO;               //  COLOR_NULO esta definido como UINT32_MAX
  }
  return G->arr_vertices[i]->color;
}

u32 GradoDelVertice(Grafo G, u32 i) {
  if (i > G->n_vertices) {
    return COLOR_NULO;               //  COLOR_NULO esta definido como UINT32_MAX
  }
  return G->arr_vertices[i]->grado;
}

u32 ColorJotaesimoVecino(Grafo G, u32 i,u32 j) {
  return G->arr_vertices[i]->arr_vecinos[j]->color;
}

u32 NombreJotaesimoVecino(Grafo G, u32 i, u32 j) {
  return G->arr_vertices[i]->arr_vecinos[j]->nombre;
}

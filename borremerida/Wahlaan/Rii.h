// Julian Merida julianmr97@gmail.com
// Julieta Borre borrejuleita@gmail.com

#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>

#include "queue.h"

// DATA:

#define COLOR_NULO UINT32_MAX

typedef unsigned int u32 ;

struct Lado {
  unsigned int v1;                        // primer vertice del lado
  unsigned int v2;                        // segundo vertice del lado
};

struct Vertice {
  u32 nombre;                             // nombre del vertice
  u32 color;                              // color del vertice
  u32 grado;                              // cantidad de vecinos del vertice
  struct Vertice **arr_vecinos;           // array de vecinos del vertice
  u32 indice;
};

struct GrafoSt {
  u32 n_vertices;                         // cantidad de vertices del grafo
  u32 m_lados;                            // cantidad de lados del grafo
  u32 cant_colores;                       // cantidad de colores con que se pinto
  struct Vertice **arr_vertices;          // array de vertices del grafo
};

typedef struct GrafoSt *Grafo;

//=============================================================================
// Funciones De Construcción/Destrucción/Copia del grafo

Grafo ConstruccionDelGrafo();
// Construye el grafo

void DestruccionDelGrafo(Grafo G);
// Destruye G y libera la memoria alocada.

Grafo CopiarGrafo(Grafo G);
/* La función aloca memoria suficiente para copiar todos los datos guardados en
G, hace una copia de G en esa memoria y devuelve un puntero a esa memoria. En
caso de no poder alocarse suficiente memoria, la función devolverá un puntero
a NULL. */

//=============================================================================
// COLOREO:

u32 Greedy(Grafo G);
// Corre greedy en G con el orden interno que debe estar guardado de alguna
// forma dentro de G. Devuelve el numero de colores que se obtiene.

int Bipartito(Grafo G);
/* Devuelve 1 si G es bipartito, 0 si no.
Si es bipartito, deja a G coloreado en forma propia con los colores 0 y 1.
Si devuelve 0, antes debe colorear a G con Greedy, en algún orden.

=============================================================================
Funciones para extraer información de datos del grafo
Las tres funciones detalladas en esta seccion deben ser O(1). */

u32 NumeroDeVertices(Grafo G);
// Devuelve el número de vértices de G.

u32 NumeroDeLados(Grafo G);
// Devuelve el número de lados de G.

u32 NumeroDeColores(Grafo G);
/* Devuelve la cantidad de colores usados en el coloreo que tiene en ese momento
G. (luego de ConstruccionDelGrafo, G siempre debe tener algún coloreo propio,
salvo en el medio de la corrida de Bipartito o de Greedy)

============================================================================
Funciones de los vertices
Las cinco funciones detalladas en esta seccion deben ser O(1). */

u32 NombreDelVertice(Grafo G, u32 i);
/* Devuelve el nombre real del vértice número i en el orden guardado en ese
momento en G. (el ı́ndice 0 indica el primer vértice, el ı́ndice 1 el
segundo, etc) Esta función no tiene forma de reportar un error (que se
producirı́a si i es mayor o igual que el número de vértices), asi que debe
ser usada con cuidado. */

u32 ColorDelVertice(Grafo G, u32 i);
/* Devuelve el color con el que está coloreado el vértice número i en el orden
guardado en ese momento en G. (el ı́ndice 0 indica el primer vértice, el
ı́ndice 1 el segundo, etc). Si i es mayor o igual que el número de vértices,
devuelve 2 32 −1. (esto nunca puede ser un color en los grafos que
testeeemos, pues para que eso fuese un color, el grafo deberia tener al menos
esa cantidad de vertices, lo cual lo hace inmanejable). */

u32 GradoDelVertice(Grafo G, u32 i);
/* Devuelve el grado del vértice número i en el orden guardado en ese momento en
G. (el ı́ndice 0 indica el primer vértice, el ı́ndice 1 el segundo, etc). Si
i es mayor o igual que el número de vértices, devuelve 2 32 − 1. (esto nunca
puede ser un grado en los grafos que testeeemos, pues para que eso fuese un
grado de algún vértice, el grafo deberia tener al menos 2 32 vertices, lo
cual lo hace inmanejable). */

u32 ColorJotaesimoVecino(Grafo G, u32 i,u32 j);
/* Devuelve el color del vécino numero j (en el orden en que lo tengan guardado
uds. en G, con el ı́ndice 0 indicando el primer vécino, el ı́ndice 1 el
segundo, etc)) del vértice número i en el orden guardado en ese momento en G.
(el ı́ndice 0 indica el primer vértice, el ı́ndice 1 el segundo, etc) */

u32 NombreJotaesimoVecino(Grafo G, u32 i,u32 j);
/* Devuelve el nombre del vécino numero j (en el orden en que lo tengan guardado
uds. en G, con el ı́ndice 0 indicando el primer vécino, el ı́ndice 1 el
segundo, etc)) del vértice número i en el orden guardado en ese momento en G.
(el ı́ndice 0 indica el primer vértice, el ı́ndice 1 el segundo, etc) Esta
función no tiene forma de reportar un error (que se producirı́a si i es mayor
o igual que el número de vértices o j es mayor o igual que el número de
vecinos de i), asi que debe ser usada con cuidado.

============================================================================
Funciones de ordenación Estas funciones cambian el orden interno guardado en
G que se usa para correr Greedy. */

char OrdenNatural(Grafo G);
/* Ordena los vertices en orden creciente de sus “nombres” reales. (recordar que
el nombre real de un vértice es un u32) retorna 0 si todo anduvo bien, 1 si
hubo algún problema. */

char OrdenWelshPowell(Grafo G);
/* Ordena los vertices de W de acuerdo con el orden Welsh-Powell, es decir, con
los grados en orden no creciente. Por ejemplo si hacemos un for en i de
GradoDelVertice(G,i) luego de haber corrido OrdenWelshPowell(G), deberiamos
obtener algo como 10,9,7,7,7,7,5,4,4. El orden relativo entre vertices que
tengan el mismo grado no se especifica, es a conveniencia de ustedes. Retorna
0 si todo anduvo bien, 1 si hubo algún problema. */

char SwitchVertices(Grafo G,u32 i,u32 j);
/* Verifica que i, j <número de vértices. Si no es cierto, retorna 1. Si ambos
estan en el intervalo permitido, entonces intercambia las posiciones de los
vertices en los lugares i y j del orden interno de G que G tenga en el
momento que se llama esta función. (numerados desde 0), y retorna 0. Por
ejemplo, si el orden interno en ese momento es 10,15,7,22,25,17,4 entonces
SwitchVertices(G,4,2) deja el orden interno 10,15,25,22,7,17,4. */

char RMBCnormal(Grafo G);
/* Si G esta coloreado con r colores y V C 1 son los vertices coloreados con 1,
V C 2 los coloreados con 2, etc, entonces esta función ordena los vertices
poniendo primero los vertices de V C 1 , luego los de V C 2 , etc, hasta V C
r−1 . En otras palabras, para todo i = 0, .., r − 2 los vertices de V C i
estan todos antes de los vertices de V C i+1 . Retorna 0 si todo anduvo bien,
1 si hubo algún problema. (pej, si allocan memoria extra temporaria para
realizar esta función y el alloc falla, deben reportar 1). */

char RMBCrevierte(Grafo G);
/* Si G esta coloreado con r colores y V C 1 son los vertices coloreados con 1,
V C 2 los coloreados con 2, etc, entonces esta función ordena los vertices
poniendo primero los vertices de V C r−1 , luego los de V C r−2 , luego los
de V C r−3 , etc, hasta V C 1 . En otras palabras, para todo i = 0, .., r − 2
los vertices de V C i estan todos luego de los vertices de V C i+1 . Retorna
0 si todo anduvo bien, 1 si hubo algún problema. (pej, si allocan memoria
extra temporaria para realizar esta función y el alloc falla, deben reportar
1). */

char RMBCchicogrande(Grafo G);
/* Si G esta coloreado con r colores y V C 1 son los vertices coloreados con 1,
V C 2 los coloreados con 2, etc, entonces esta función ordena los vertices
poniendo primero los vertices de V C j 1 , luego los de V C j 2 , etc, donde
j 1 , j 2 , ..., j r son tales que |V C j 1 | ≤ |V C j 2 | ≤ ... ≤ |V C j r |
Retorna 0 si todo anduvo bien, 1 si hubo algún problema. (pej, si se alloca
memoria extra temporaria para realizar esta función y el alloc falla, deben
reportar 1). */

char SwitchColores(Grafo G, u32 i, u32 j);
/* Verifica que i, j <número de colores que tiene G en ese momento. Si no es
cierto, retorna 1. Si ambos estan en el intervalo permitido, entonces
intercambia los colores i, j: todos los vertices que esten coloreados en el
coloreo actual con i pasan a tener 7el color j en el nuevo coloreo y los que
estan coloreados con j en el coloreo actual pasan a tener el color i en el
nuevo coloreo. Los demas colores quedan como están. Retorna 0 si todo se hizo
bien. */

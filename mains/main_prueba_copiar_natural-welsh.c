#include "Rii.h"

int main() {
    // Grafo hola = NULL;
      Grafo G = ConstruccionDelGrafo();
      if (G == NULL) {
          return 1;
      }


      u32 result = Greedy2(G);
      printf("GREEDYYYYY  %u\n", result);

      Grafo grafo2 = CopiarGrafo(G);
      Grafo grafo3 = CopiarGrafo(G);


      printf("%s\n", "Ahora vamos con grafo2 ");
      printf("%s\n", "Sus datos son: ");

      for (u32 i = 0; i < grafo2->n_vertices; i++) {
          printf("%u ", NombreDelVertice(grafo2,i));
          printf("%u ", ColorDelVertice(grafo2,i));
          printf("%u\n", GradoDelVertice(grafo2,i));
      }

      printf("%s\n", "Antes de OrdenWelshPowell");

      char Welsh = OrdenWelshPowell(grafo2);
      printf("Welsh es %c\n", Welsh);

      printf("%s\n", "Despues de OrdenWelshPowell");

      for (u32 i = 0; i < grafo2->n_vertices; i++) {
          printf("%u ", NombreDelVertice(grafo2,i));
          printf("%u ", ColorDelVertice(grafo2,i));
          printf("%u\n", GradoDelVertice(grafo2,i));
      }

      printf("%s\n", "Ahora vamos con grafo3 ");
      printf("%s\n", "Sus datos son: ");

      for (u32 i = 0; i < grafo3->n_vertices; i++) {
          printf("%u ", NombreDelVertice(grafo3,i));
          printf("%u ", ColorDelVertice(grafo3,i));
          printf("%u\n", GradoDelVertice(grafo3,i));
      }

      char Natural = OrdenNatural(grafo3);
      printf("Natural es %c\n", Natural);

      for (u32 i = 0; i < grafo3->n_vertices; i++) {
          printf("%u ", NombreDelVertice(grafo3,i));
          printf("%u ", ColorDelVertice(grafo3,i));
          printf("%u\n", GradoDelVertice(grafo3,i));
      }

      Welsh = OrdenWelshPowell(grafo3);
      printf("Welsh es %c\n", Welsh);

      for (u32 i = 0; i < grafo3->n_vertices; i++) {
          printf("%u ", NombreDelVertice(grafo3,i));
          printf("%u ", ColorDelVertice(grafo3,i));
          printf("%u\n", GradoDelVertice(grafo3,i));
      }

    // printf("CHAR %c\n", OrdenNatural(G));

    //
    // printf("ORIGINAL 5 %u\n", G->arr_vertices[5]->nombre);
    //
    // printf("NOMBRE ORIGINAL 5 PRIMER VECINO %u\n", G->arr_vertices[5]->arr_vecinos[0]->nombre);
    // printf("GRADOOOO %u\n", G->arr_vertices[5]->arr_vecinos[0]->grado);
    // // printf("NOMBRE ORIGINAL 5 PRIMER VECINO %u\n", G->arr_vertices[11]->arr_vecinos[1]->nombre);
    // printf("COLOR ORIGINAL 5 PRIMER VECINO %u\n", G->arr_vertices[20]->arr_vecinos[0]->color);
    // printf("GRADOOOO %u\n", G->arr_vertices[20]->arr_vecinos[0]->grado);
    // printf("%s\n", "PASOOOOOOOOOOOOOOO");

    // // printf("ORIGINAL 5 GRADO %u\n", G->arr_vertices[5]->grado);
    // printf("ORIGINAL 20 %u\n", G->arr_vertices[20]->nombre);
    // printf("COLOR ORIGINAL 20 PRIMER VECINO %u\n", G->arr_vertices[20]->arr_vecinos[0]->nombre);
    // // printf("ORIGINAL 20 GRADO %u\n", G->arr_vertices[20]->grado);
    // printf("LALALALALALALALAL  %c\n", SwitchVertices(G, 5, 20));
    //
    // printf("NUEVO 5 %u\n", G->arr_vertices[5]->nombre);
    // printf("COLOR NUEVO 5 PRIMER VECINO %u\n", G->arr_vertices[5]->arr_vecinos[0]->nombre);
    // // printf("NUEVO 5 GRADO %u\n", G->arr_vertices[5]->grado);
    // printf("NUEVO 20 %u\n", G->arr_vertices[20]->nombre);
    // printf("COLOR NUEVO 20 PRIMER VECINO %u\n", G->arr_vertices[20]->arr_vecinos[0]->nombre);
    // printf("NUEVO 20 GRADO %u\n", G->arr_vertices[20]->grado);
    DestruccionDelGrafo(G);
    DestruccionDelGrafo(grafo2);
    DestruccionDelGrafo(grafo3);
    // }
    // if (hola) {
    //   DestruccionDelGrafo(hola);
    // }
    return 0;
}

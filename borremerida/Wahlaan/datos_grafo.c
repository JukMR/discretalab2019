#include "Rii.h"

u32 NumeroDeVertices(Grafo G) {
  return G->n_vertices;
}

u32 NumeroDeLados(Grafo G) {
  return G->m_lados;
}

u32 NumeroDeColores(Grafo G) {
  return G->cant_colores;
}

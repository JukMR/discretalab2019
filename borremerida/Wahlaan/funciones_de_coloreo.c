#include "Rii.h"
#include "queue.h"

u32 Greedy(Grafo G) {
  u32 color_nulo = COLOR_NULO;    //  Variable para indicar que un vertice no esta coloreado
  u32 contador_colores = 0;       //  Contador para indicar con cuantos colores coloreo greedy
  u32 reseteo = 0;                //  Variable para limpiar el arreglo de booleanos
  u32 k = 0;
  bool *usados = calloc(G->n_vertices, sizeof(bool));

  //  Compruebo si se aloco bien la memoria, en caso contrario libero todo lo
  //  que haya alocado hasta el mometo y devuelvo NULL
  if (usados == NULL) {
    printf("%s\n", "error al alocar memoria");
    return 0;
  }
  //  Arreglo para indicar que colores ya estan en uso

  for (u32 i = 0; i < G->n_vertices; i++) {
    usados[i] = false;
  }

  //  Seteo el color de todos los vertices como nulo
  for (u32 i = 0; i < G->n_vertices; i++) {
    G->arr_vertices[i]->color = color_nulo;
  }

  G->arr_vertices[0]->color = 0;    //  Seteo el color del primer vertice en 0

  //  Itero sobre todos los vertices del grafo
  for (u32 i = 1; i < G->n_vertices; i++) {
    //  Itero sobre los vecinos de cada vertice
    for (u32 j = 0; j < G->arr_vertices[i]->grado; j++) {
      if (G->arr_vertices[i]->arr_vecinos[j]->color != color_nulo) {
        //  Si el vecino j-esimo esta coloreado lo marco como usado en el array de colores usados
        usados[G->arr_vertices[i]->arr_vecinos[j]->color] = true;
        if (G->arr_vertices[i]->arr_vecinos[j]->color > reseteo) {
          //  Actualizo la variable de reseteo del array de colores usados
          reseteo = G->arr_vertices[i]->arr_vecinos[j]->color;
        }
      }
    }

    //  Busco el primer color que no este en uso
    while (usados[k]) {
      k++;
    }

    G->arr_vertices[i]->color = k;    //  Le asigo el color que encontré al vertice
    if (contador_colores < k) {
      contador_colores = k;           //  Actualizo la cantidad de colores de greedy
    }

    k = 0;

    //  Reseteo el array de colores usados
    for (u32 j = 0; j <= reseteo; j++) {
      usados[j] = false;
    }
  }

  G->cant_colores = contador_colores + 1;   //  Le asigno al grafo la cantidad de colores con la que lo coloreé
  free(usados);
  return G->cant_colores;
}


int Bipartito(Grafo G) {
  u32 color_nulo = COLOR_NULO;  //  Variable para indicar que un vertice no esta coloreado

  //  Seteo el color de todos los vertices como nulo y actualizo el indice de cada vertice
  for (u32 i = 0; i < G->n_vertices; i++) {
    G->arr_vertices[i]->color = color_nulo;
    G->arr_vertices[i]->indice = i;
  }

  queue_t q = create_queue();   //  Creo una cola

  //  Itero sobre todos los vertices del grafo
  for (u32 k = 0; k < G->n_vertices; k++) {
    if (G->arr_vertices[k]->color == color_nulo) {
      /*  Si el color del vertice es el color nulo quiere decir que no
      lo visité por lo tanto encolo el indice del vertice y seteo el
      color del vertice en 0  */
      enqueue(q, G->arr_vertices[k]->indice);
      G->arr_vertices[k]->color = 0;
      while (!is_empty_queue(q)) {
        queue_elem temp = first(q);   //  Obtengo el primer elemento de la cola y lo asigno a una variable temporal
        dequeue(q);                   //  Decolo el primer elemento
        //  Itero sobre los vecinos del vertice con índice temp
        for (u32 i = 0; i < G->arr_vertices[temp]->grado; i++) {
          if (G->arr_vertices[temp]->arr_vecinos[i]->color == color_nulo) {
            // Si el color del vecino i-esimo del vertice es nulo lo seteo en
            // el opuesto al del vertice y encolo el indice
            G->arr_vertices[temp]->arr_vecinos[i]->color =
            1 - G->arr_vertices[temp]->color;
            enqueue(q, G->arr_vertices[temp]->arr_vecinos[i]->indice);
          } else if (G->arr_vertices[temp]->color ==
                     G->arr_vertices[temp]->arr_vecinos[i]->color) {
            // Si el color del vecino i-esimo es el mismo al del vertice el
            // grafo no es bipartito. Lo coloreo con greedy y devuelvo 0
            u32 greedy = Greedy(G);
            return 0;
          }
        }
      }
    }
  }
  G->cant_colores = 2;
  destroy_queue(q);
  return 1;
}

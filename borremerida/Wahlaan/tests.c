// #include <stdio.h>
// #include <stdlib.h>
// #include <stdbool.h>
// #include <stdint.h>
// #include <assert.h>
// #include <time.h>
//
// #include "Rii.h"
//
// #define NULL_COLOR UINT32_MAX
//
// #define MAX(a, b) (((a) > (b)) ? (a) : (b))
// #define MIN(a, b) (((a) < (b)) ? (a) : (b))
//
// #define timeit(CALL) \
//   printf("Running %s... ", #CALL); \
//   fflush(stdout); \
//   t = clock(); \
//   (CALL); \
//   t = clock() - t; \
//   time_taken = ((double) t) / CLOCKS_PER_SEC; \
//   printf("done in %f seconds\n", time_taken); \
//
// #define ignoreit(CALL) false && (CALL);
//
// typedef uint32_t u32;
//
// // Checks that CopiarGrafo(g) == g
// static bool test_graph_copy(Grafo g) {
//   Grafo h = CopiarGrafo(g);
//   assert(NumeroDeVertices(g) == NumeroDeVertices(h));
//   assert(NumeroDeLados(g) == NumeroDeLados(h));
//   assert(NumeroDeColores(g) == NumeroDeColores(h));
//   for (u32 i = 0; i < NumeroDeVertices(g); i++) {
//     assert(NombreDelVertice(g, i) == NombreDelVertice(h, i));
//     assert(ColorDelVertice(g, i) == ColorDelVertice(h, i));
//     assert(GradoDelVertice(g, i) == GradoDelVertice(h, i));
//     for (u32 j = 0; j < GradoDelVertice(g, i); j++) {
//       assert(ColorJotaesimoVecino(g, i, j) == ColorJotaesimoVecino(h, i, j));
//       assert(NombreJotaesimoVecino(g, i, j) == NombreJotaesimoVecino(h, i, j));
//     }
//   }
//   return true;
// }
//
// // Pre: g is complete
// // Checks that greedy gives ccount == n for g == kn
// static bool test_kn(Grafo g) {
//   assert(NumeroDeVertices(g) == Greedy(g) && NumeroDeVertices(g) == NumeroDeColores(g));
//   return true;
// }
//
// // Pre: g is bipartite
// // Checks that bipartite gives ccount == 2
// static bool test_bipartite(Grafo g) {
//   assert(Bipartito(g) == 1 && 2 == NumeroDeColores(g));
//   return true;
// }
//
// // Checks that greedy gives ccount <= DELTA + 1
// // Checks that greedy gives ccount <= DELTA if g != kn or odd cycle
// static bool test_greedy(Grafo g) {
//   bool is_odd_cycle = false; // HACK: For now change this to true when testing with an odd_cycle
//   u32 delta = 0;
//   for (u32 i = 0; i < NumeroDeVertices(g); i++)
//     delta = MAX(GradoDelVertice(g, i), delta);
//
//   u32 n = NumeroDeVertices(g);
//   bool is_kn = n * (n - 1) / 2 == NumeroDeLados(g);
//
//   Greedy(g);
//
//   if (is_kn) {
//     assert(NumeroDeColores(g) == n && n == delta + 1);
//   } else if (is_odd_cycle) {
//     assert(NumeroDeColores(g) == 3 && 3 == delta + 1);
//   } else {
//     assert(NumeroDeColores(g) <= delta);
//   }
//
//   return true;
// }
//
// // Tests that g[i].id < g[j].id forall i < j
// static bool test_natural_order(Grafo g) {
//   OrdenNatural(g);
//   u32 n = NumeroDeVertices(g);
//   u32 cid = NombreDelVertice(g, 0);
//   u32 i;
//   for (i = 1; i < n && cid < NombreDelVertice(g, i); i++)
//     cid = NombreDelVertice(g, i);
//   assert(i == n && "Natural order test failed");
//   return true;
// }
//
// // Tests that g[i].r < g[j].r forall i < j
// static bool test_welsh_powell_order(Grafo g) {
//   OrdenWelshPowell(g);
//   u32 n = NumeroDeVertices(g);
//   u32 cr = GradoDelVertice(g, 0);
//   u32 i;
//   for (i = 1; i < n && cr >= GradoDelVertice(g, i); i++)
//     cr = GradoDelVertice(g, i);
//   assert(i == n && "Welsh powell order test failed");
//   return true;
// }
//
// // Tests that g[i].color <= g[j].color forall i < j
// static bool test_RMBC_order(Grafo g) {
//   RMBCnormal(g);
//   u32 n = NumeroDeVertices(g);
//   u32 cc = ColorDelVertice(g, 0);
//   u32 i;
//   for (i = 1; i < n && cc <= ColorDelVertice(g, i); i++)
//     cc = ColorDelVertice(g, i);
//   assert(i == n && "RMBC order test failed");
//   return true;
// }
//
// // Tests that g[i].color >= g[j].color forall i < j
// static bool test_RMBC_reverse_order(Grafo g) {
//   RMBCrevierte(g);
//   u32 n = NumeroDeVertices(g);
//   u32 cc = ColorDelVertice(g, 0);
//   u32 i;
//   for (i = 1; i < n && cc >= ColorDelVertice(g, i); i++)
//     cc = ColorDelVertice(g, i);
//   assert(i == n && "RMBC reverse order test failed");
//   return true;
// }
//
// // let voc c = #vertices_of_color(c);
// // Tests that voc(g[i].c) <= voc(g[j].c) forall i < j
// static bool test_RMBC_ccardinal_order(Grafo g) {
//   u32 cc, *voc;
//   cc = NumeroDeColores(g);
//   voc = (u32*) calloc(cc, sizeof(u32));
//   for (u32 i = 0; i < NumeroDeVertices(g); i++)
//     voc[ColorDelVertice(g, i)]++;
//
//   RMBCchicogrande(g);
//
//   u32 n = NumeroDeVertices(g);
//   u32 cvoc = voc[ColorDelVertice(g, 0)];
//   u32 i;
//   for (i = 1; i < n && cvoc <= voc[ColorDelVertice(g, i)]; i++)
//     cvoc = voc[ColorDelVertice(g, i)];
//   assert(i == n && "RMBC ccardinal order test failed");
//
//   free(voc);
//   return true;
// }
//
// // Tests that switching the first and last colors works as expected
// static bool test_switch_colores(Grafo g) {
//   Grafo h = CopiarGrafo(g);
//   u32 i = 0;
//   u32 j = NumeroDeColores(g) - 1;
//   SwitchColores(g, i, j);
//   for (u32 k = 0; k < NumeroDeVertices(g); k++) {
//     if (ColorDelVertice(g, k) == i) assert(ColorDelVertice(h, k) == j);
//     else if (ColorDelVertice(g, k) == j) assert(ColorDelVertice(h, k) == i);
//   }
//   return true;
// }
//
// // Tests with instruction recommendations
// static bool test_penazzi(Grafo g) {
//   u32 VERTEX_SWITCH_N = 100;
//   u32 RMBC_N = 1000;
//
//   Grafo h, temp;
//   h = CopiarGrafo(g);
//   OrdenNatural(g); OrdenWelshPowell(h);
//
//   if (Greedy(g) > Greedy(h)) {
//     temp = h; h = g; g = temp;
//   }
//   DestruccionDelGrafo(h);
//   printf("\n");
//   printf("\tInitial cc %u\n", NumeroDeColores(g));
//
//   srand(time(NULL));
//   u32 r, s, n = NumeroDeVertices(g);
//   for (u32 i = 0; i <= VERTEX_SWITCH_N; i++) {
//     r = rand() % n; s = rand() % n;
//
//     h = CopiarGrafo(g);
//     SwitchVertices(h, r, s);
//
//     if (NumeroDeColores(g) > Greedy(h)) {
//       temp = h; h = g; g = temp;
//     }
//     DestruccionDelGrafo(h);
//   }
//   printf("\tAfter %u vertex switch cc %u\n", VERTEX_SWITCH_N, NumeroDeColores(g));
//
//   u32 sw, cc;
//   for (u32 i = 0; i <= RMBC_N; i++) {
//     cc = NumeroDeColores(g);
//     sw = rand() % 3;
//     switch (sw) {
//       case 0: RMBCrevierte(g);
//               break;
//       case 1: RMBCchicogrande(g);
//               break;
//       default: r = rand() % cc;
//                s = rand() % cc;
//                SwitchColores(g, r, s);
//                RMBCnormal(g);
//     }
//     //printf("\ti: %u\tsw: %u\tcc: %u\tgreedy: %u\n", i, sw, cc, cc2);
//     assert(cc >= Greedy(g));
//     if (i % 50 == 0) printf("\tRMBC vuelta %u. cc %u\n", i, NumeroDeColores(g));
//   }
//   printf("\tAfter %u RMBC cc %u\n\t", RMBC_N, NumeroDeColores(g));
//
//   return true;
// }
//
// int main(int argc, char* argv[]) {
//   clock_t t;
//   double time_taken;
//   Grafo g;
//
//   timeit(g = ConstruccionDelGrafo());
//   if (!g) return EXIT_FAILURE;
//
//   timeit(test_graph_copy(g));
//   ignoreit(test_kn(g));
//   ignoreit(test_bipartite(g));
//   timeit(test_greedy(g));
//   timeit(test_natural_order(g));
//   timeit(test_welsh_powell_order(g));
//   timeit(test_RMBC_order(g));
//   timeit(test_RMBC_reverse_order(g));
//   timeit(test_switch_colores(g));
//   timeit(test_RMBC_ccardinal_order(g));
//   ignoreit(test_penazzi(g));
//
//   timeit(DestruccionDelGrafo(g));
//
//   printf("All tests passed\n");
//   return EXIT_SUCCESS;
// }

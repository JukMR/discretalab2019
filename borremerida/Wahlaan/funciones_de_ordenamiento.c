#include "Rii.h"

//  Funcion que de comparación va a utilizar qsort() para ordenar con orden natural
static int CompararNatural(const void * left, const void * right) {
    struct Vertice *a = *((struct Vertice **) left);
    struct Vertice *b = *((struct Vertice **) right);
    if (a->nombre < b->nombre) {
        return -1;
    } else if (a->nombre > b->nombre) {
        return 1;
    } else {
        return 0;
    }
 }

 char OrdenNatural(Grafo G) {
   qsort(G->arr_vertices, G->n_vertices, sizeof(struct Vertice*),
         CompararNatural);
   return '0';
 }

 //  Funcion que de comparación va a utilizar qsort() para ordenar con orden Welsh Powell
static int CompararWelsh(const void * left, const void * right) {
  struct Vertice *a = *((struct Vertice **) left);
  struct Vertice *b = *((struct Vertice **) right);
  if (a->grado > b->grado) {
      return -1;
  } else if (a->grado < b->grado) {
      return 1;
  } else {
      return 0;
  }
}

char OrdenWelshPowell(Grafo G) {
  qsort(G->arr_vertices, G->n_vertices, sizeof(struct Vertice*), CompararWelsh);
  return '0';
}

//  Funcion que de comparación va a utilizar qsort() para ordenar con orden RMBC normal
static int CompararRMBCnormal(const void * left, const void * right) {
  struct Vertice *a = *((struct Vertice **) left);
  struct Vertice *b = *((struct Vertice **) right);
  if (a->color < b->color) {
      return -1;
  } else if (a->color > b->color) {
      return 1;
  } else {
      return 0;
  }
}

char RMBCnormal(Grafo G) {
  qsort(G->arr_vertices, G->n_vertices, sizeof(struct Vertice*),
        CompararRMBCnormal);
  return '0';
}

//  Funcion que de comparación va a utilizar qsort() para ordenar con orden RMBC invertido
static int CompararRMBCrevierte(const void * left, const void * right) {
  struct Vertice *a = *((struct Vertice **) left);
  struct Vertice *b = *((struct Vertice **) right);
  if (a->color > b->color) {
      return -1;
  } else if (a->color < b->color) {
      return 1;
  } else {
      return 0;
  }
}

char RMBCrevierte(Grafo G) {
  qsort(G->arr_vertices, G->n_vertices, sizeof(struct Vertice*),
        CompararRMBCrevierte);
  return '0';
}

char SwitchColores(Grafo G,u32 i,u32 j) {
  //  Chequeo que ambos colores sean menores a la cantidad de colores con la que
  //  esta coloreado mi grafo
  if (i < G->cant_colores && j < G->cant_colores) {
    //  Itero sobre la cantidad de vertices
    for (u32 k = 0; k < G->n_vertices; k++){
      if (G->arr_vertices[k]->color == i) {
        //  A cada vertice coloreado con i lo coloreo con j
        G->arr_vertices[k]->color = j;
      } else if (G->arr_vertices[k]->color == j) {
        //  A cada vertice coloreado con j lo coloreo con i
        G->arr_vertices[k]->color = i;
      }
    }
    return '0';
  } else {
    //  Si alguno de los colores es mayor a la cantidad de colores devuelvo 1
    return '1';
  }
}

char SwitchVertices(Grafo G,u32 i,u32 j) {
  if (i < G->n_vertices && j < G->n_vertices) {
    //  Chequeo que ambos vertices sean menores a la cantidad de vertices que
    //  tiene mi grafo
    struct Vertice *aux = G->arr_vertices[j];   //  Guardo el vertice j en una variable auxiliar
    G->arr_vertices[j] = G->arr_vertices[i];    //  Cambio el vertice j por el i
    G->arr_vertices[i] = aux;                   //  Cambio el vertice i por el auxiliar
    return '0';
  } else {
    return '1';
  }
}
